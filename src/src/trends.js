
import Test from './testData';

import moment from 'moment';
import axios from 'axios';

export default {

  setTrends(tkn, rId){
    return new Promise((resolve, reject) => {
      axios.get('https://api-matt.breezi.net/trends/rooms/' + rId, {headers: {'Content-Type': 'application/json', Authorization: tkn}})
        .then(trends => {
          if (trends.data.data.length < 5) {
            resolve(noTrendData());
          } else {
            let series = [];
            trends = trends.data.data.map(t => {
              return {
                ttp: t.meas.ttp,
                hhm: t.meas.hhm,
                iaq: t.meas.iaq,
                tms: t.tms
              }
            }).sort((a, b) => {return a.tms - b.tms});
            console.log(trends);
            let colours = ['#4ddf5a', '#e47f27', '#03acdc', '#f7de2d', '#f60021'];
            series.push(setAsyncSeries(trends, 'ttp', '#c93a1f'));
            series.push(setAsyncSeries(trends, 'hhm', '#298bbe'));
            series.push(setAsyncSeries(trends, 'iaq', '#3db536'));
            series = series.map((s, x) => {
              s.color = colours[x];
              return s;
            });
            let categories = setCats(trends);
            resolve({
              categories: categories,
              series: series,
              async: true
            });
          }
        }).catch(err => {
          console.log({ERROR: err})
          resolve(noTrendData());
        })
    });

    function noTrendData(){
      let trends = Test();

      trends = trends.sort((a, b) => {return a.tms - b.tms});

      let categories = setCats(trends);
      let series = [];

      series.push(setSeries(trends, 'days'));
      series.push(setSeries(trends, 'ttp'));
      series.push(setSeries(trends, 'hhm'));
      series.push(setSeries(trends, 'iaq'));
      series.push(setSeries(trends, 'co2'));
      let colours = ['#4ddf5a', '#e47f27', '#7f3be0', '#f7de2d', '#f60021'];
      series = series.map((s, x) => {
        // s.color = colours[x];
        s.color = '#777e74';
        return s;
      });

      return {
        categories: categories,
        series: series,
        async: false
      }
    }
  }

}

function setCats(trends){
  console.log(trends);
  let cats = trends.map(t => moment(t.tms * 1000).format('L').split('/'));
  return cats.map(c => `${c[0]}/${c[1]}`);
}

function setAsyncSeries(trends, prop){
  let trend = switchName(prop);
  trend.data = trends.map(t => t[prop]);
  return trend;
}

function setSeries(trends, prop, color){
  let trend = {
    name: switchName(prop).name,
    tooltip: switchName(prop).tooltip
  }
  if (prop === 'days'){
    trend.data = trends.map(t => t.dap.days);
  } else {
    trend.data = trends.map(t => t.meas[prop]);
  }
  return trend;
}

function switchName(prop){
  switch (prop) {
    case 'days':
      return {name: 'Days remaining', tooltip: {valueSuffix: 'd'}};
    break;
    case 'ttp':
      return {name: 'Temperature', tooltip: {valueSuffix: ' ˚C'}};
    break;
    case 'hhm':
      return {name: 'Humidity', tooltip: {valueSuffix: ' %'}};
    break;
    case 'iaq':
      return {name: 'tVOC', tooltip: {valueSuffix: ''}};
    break;
    case 'co2':
      return {name: 'CO2', tooltip: {valueSuffix: 'ppm'}};
    break;
  }
}
